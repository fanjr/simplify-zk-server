package org.fanjr.simplify.zk.server;

import org.springframework.boot.autoconfigure.ImportAutoConfiguration;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.context.annotation.Configuration;

@Configuration
@ImportAutoConfiguration(ZkServerConfiguration.class)
@ConditionalOnMissingBean(ZkServerBean.class)
public class ZkServerAutoConfiguration {

}
