# Simplify-zk-server

#### What is this
A simple embedded zk service package that is out-of-the-box!<br/>
Let the application start automatically start an embedded Zookeeper service that can be accessed externally。<br/>


#### he effect is as follows
##### Using Springboot
Introduce the module and use it

```
<dependency>
	<groupId>org.fanjr.simplify</groupId>
	<artifactId>simplify-zk-server-starter</artifactId>
	<version>1.0.0</version>
</dependency>
```

##### Just using spring
Introduction module

```
<dependency>
	<groupId>org.fanjr.simplify</groupId>
	<artifactId>simplify-zk-server-core</artifactId>
	<version>1.0.0</version>
</dependency>
```

Use annotations @EnableZkServer or configure Springbeans in the bean configuration XML

```
<bean class="org.fanjr.simplify.zk.server.ZkServerConfiguration" />
```

#### Parameter specification
All of the following parameters can be used with the -d injection parameter when you start the JVM, or you can also use the spring identifiers properties file Settings directly

```
#Use the native zookeeper parameter file(zoo.cfg)
zookeeper.cfg_path=

#Print logo
#  ___ _            _ _  __         _____       ___                     
# / __(_)_ __  _ __| (_)/ _|_  _ __|_  | |_____/ __|___ _ ___ _____ _ _ 
# \__ | | '  \| '_ | | |  _| || |___/ /| / |___\__ / -_| '_\ V / -_| '_|
# |___|_|_|_|_| .__|_|_|_|  \_, |  /___|_\_\   |___\___|_|  \_/\___|_|  
#             |_|           |__/                                        
zookeeper.simplify.enableLogo=true

#Or use the following parameters to configure them, and use the default.
zookeeper.clientPort=2181
zookeeper.dataDir=zkData
zookeeper.dataLogDir=zkData
zookeeper.maxSessionTimeout=-1
zookeeper.minSessionTimeout=-1
zookeeper.syncEnabled=true
zookeeper.syncLimit=0
zookeeper.initLimit=0
zookeeper.tickTime=3000
zookeeper.clientPortAddress=127.0.0.1
zookeeper.maxClientCnxns=60

```



