# Simplify-zk-server

#### 介绍
一种开箱即用的简易的嵌入式ZK服务包<br/>
让应用启动时自动启动一个内嵌的zookeeper服务，可以被外部访问。<br/>
具体有什么用自行摸索(手动狗头

#### 使用说明
##### 使用Springboot 
引入模块，即可使用

```
<dependency>
	<groupId>org.fanjr.simplify</groupId>
	<artifactId>simplify-zk-server-starter</artifactId>
	<version>1.0.0</version>
</dependency>
```

##### 仅使用Spring
引入模块

```
<dependency>
	<groupId>org.fanjr.simplify</groupId>
	<artifactId>simplify-zk-server-core</artifactId>
	<version>1.0.0</version>
</dependency>
```

然后用注解@EnableZkServer打开 或者 在Bean配置xml中配置Springbean如下

```
<bean class="org.fanjr.simplify.zk.server.ZkServerConfiguration" />
```

#### 参数说明
以下所有参数均可以在启动JVM时使用-D注入参数,也可以直接使用Spring可识别的Properties文件设置

```
#使用原生的Zookeeper配置文件(zoo.cfg)
zookeeper.cfg_path=

#启动时控制台输出logo,主要用于提醒你正在使用内嵌的zk,不喜欢可以设置成false关闭。
#  ___ _            _ _  __         _____       ___                     
# / __(_)_ __  _ __| (_)/ _|_  _ __|_  | |_____/ __|___ _ ___ _____ _ _ 
# \__ | | '  \| '_ | | |  _| || |___/ /| / |___\__ / -_| '_\ V / -_| '_|
# |___|_|_|_|_| .__|_|_|_|  \_, |  /___|_\_\   |___\___|_|  \_/\___|_|  
#             |_|           |__/                                        
zookeeper.simplify.enableLogo=true

#或者使用下面的参数进行配置，未设置则以下面内容为默认值。
zookeeper.clientPort=2181
zookeeper.dataDir=zkData
zookeeper.dataLogDir=zkData
zookeeper.maxSessionTimeout=-1
zookeeper.minSessionTimeout=-1
zookeeper.syncEnabled=true
zookeeper.syncLimit=0
zookeeper.initLimit=0
zookeeper.tickTime=3000
zookeeper.clientPortAddress=127.0.0.1
zookeeper.maxClientCnxns=60

```



