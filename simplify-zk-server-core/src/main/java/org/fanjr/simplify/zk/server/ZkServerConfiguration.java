package org.fanjr.simplify.zk.server;

import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.BeanFactoryPostProcessor;
import org.springframework.beans.factory.config.ConfigurableListableBeanFactory;
import org.springframework.context.EnvironmentAware;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.core.env.Environment;

@Configuration
@Order(Ordered.LOWEST_PRECEDENCE)
public class ZkServerConfiguration implements BeanFactoryPostProcessor, EnvironmentAware {

	private ZkServerBean bean;

	@Bean
	public ZkServerBean zkServerBean() {
		return bean;
	}

	@Override
	public void setEnvironment(Environment environment) {
		if (environment.getProperty("zookeeper.simplify.enableLogo", boolean.class, true)) {
			printLogo();
		}
		bean = new ZkServerBean(environment);
	}

	@Override
	public void postProcessBeanFactory(ConfigurableListableBeanFactory beanFactory) throws BeansException {
		// skip
	}

	private void printLogo() {
		System.out.println("  ___ _            _ _  __         _____       ___                     \r\n"
				+ " / __(_)_ __  _ __| (_)/ _|_  _ __|_  | |_____/ __|___ _ ___ _____ _ _ \r\n"
				+ " \\__ | | '  \\| '_ | | |  _| || |___/ /| / |___\\__ / -_| '_\\ V / -_| '_|\r\n"
				+ " |___|_|_|_|_| .__|_|_|_|  \\_, |  /___|_\\_\\   |___\\___|_|  \\_/\\___|_|  \r\n"
				+ "             |_|           |__/                                        ");
	}

}
