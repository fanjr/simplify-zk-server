package org.fanjr.simplify.zk.server;

import static java.lang.annotation.ElementType.TYPE;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import org.springframework.context.annotation.Import;

@Retention(RUNTIME)
@Target({ TYPE })
@Import(ZkServerConfiguration.class)
public @interface EnableZkServer {

}
